import React, { useState, useEffect } from "react"
import { Link, graphql } from "gatsby"
import styled from "styled-components"
import Logo from "../images/assets/Logo.svg"
import MenuIcon from "../images/assets/MenuIcon.svg"
import ScrollIcon from "../images/assets/ScrollDown.svg"
import OurRoots from "../components/Page/OurRoots.jsx"
import Footer from "../components/Shared/Footer"
import MenuData from "../components/Shared/Menu"
import Swiper from "../components/Page/Swiper"
import SEO from "../components/Shared/SEO"
import "animate.css/animate.css"
// import 'animate.min.css';
import "../styles/font.scss"

const Root = styled.div`
  display: block;
  overflow-x: hidden;
  height: 100%;
`
const LogoDiv = styled(Link)`
  position: absolute;
  top: 10%;
  left: 6%;
`
const MenuDiv = styled.div`
  position: absolute;
  top: 10%;
  right: 6%;
  height: 56px;
  align-items: center;
  display: flex;
  cursor: pointer;
`
const ScrollDownLink = styled(Link)`
  display: inline-block;
  position: absolute;
  bottom: 11vh;
  left: calc(50% - 24px);
  cursor: pointer;
  @media (max-width: 800px) {
    left: calc(85% - 24px);
    ${"" /* #Scroll {
      display: none;
    } */}
  }
`
const Menu = styled.div`
  position: absolute;
  right: ${props => (props.sown ? 0 : "-800px")};
  transition: right 0.5s;
  top: 0;
  height: 100vh;
  width: 467px;
  background-color: #14415a;
  z-index: 2;
  @media (max-width: 800px) {
    width: 100vw;
  }
`
const HeadTitle = styled.div`
  position: absolute;
  color: white;
  font-size: 68px;
  line-height: 90px;
  font-family: Nunito Sans;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  height: max-content;
  max-width: 749px;
  text-align: center;
  @media (max-width: 800px) {
    font-size: 48px;
    line-height: 64px;
    max-width: 400px;
  }
  @media (max-width: 420px) {
    font-size: 28px;
    line-height: 37px;
    max-width: 245px;
  }
`
const ShortDetail = styled.div`
  font-size: 24px;
  color: #aaa599;
  max-width: 801px;
  margin: auto;
  ${"" /* margin-bottom: 60px;
  margin-top: 60px; */}
  line-height: 1.7;
  padding: 145px 0;
  font-family: Nunito Sans;
  color: #aaa599;
  @media (max-width: 800px) {
    max-width: 690px;
    ${"" /* margin-left: 30px;
    margin-right: 30px; */}
    padding: 110px 80px;
    margin: 0 auto;
    font-size: 24px;
  }
  @media (max-width: 500px) {
    font-size: 18px;
    max-width: 100%;
    padding: 90px 40px;
  }
`

const PageTemplate = ({ data, location }) => {
  const [menu, setMenu] = useState(false)
  const content = data.markdownRemark.frontmatter
  const seoTitle = content?.seo?.title || content?.title || "Title"
  const [isOpenMobileMenu, setStateMobileMenu] = useState(false)
  const [chaseCurrentSlide, setChaseCurrentSlide] = useState(1)
  const [title, setTitle] = useState("")

  const chaseSlideHandler = index => {
    setChaseCurrentSlide(index)
    if (chaseCurrentSlide === 1) {
      setTitle("Maāhris Restoration brings heritage to life.")
    }
    if (chaseCurrentSlide === 2) {
      setTitle("Devoted to detail and expert craftsmanship.")
    }
    if (chaseCurrentSlide === 3) {
      setTitle("Preserve the remarkable history and tradition .")
    }
  }

  useEffect(() => {
    chaseSlideHandler(chaseCurrentSlide)
  }, [chaseCurrentSlide])

  const scrollHandler = e => {
    e.target.addEventListener("click", function (e) {
      e.preventDefault()
      const blockID = e.target.getAttribute("href")

      document.querySelector(blockID).scrollIntoView({
        behavior: "smooth",
        block: "start",
      })
    })
  }

  const handleStateMobileMenu = () => {
    setStateMobileMenu(!isOpenMobileMenu)
    const body = document.querySelector("body")

    if (isOpenMobileMenu) {
      body.style.overflow = "auto"
    } else {
      body.style.overflow = "hidden"
    }
  }

  useEffect(() => {
    if (!isOpenMobileMenu) {
      const body = document.querySelector("body")
      body.style.overflow = "auto"
    }
  }, [isOpenMobileMenu])

  return (
    <>
      <SEO
        title={seoTitle}
        description={content?.seo?.description}
        image={content?.seo?.imageUrl}
        location={location.href}
      />
      <Root>
        <div className="max-h-screen relative center white">
          <Swiper chaseSlideHandler={chaseSlideHandler} content={content} />

          <LogoDiv to="/">
            <Logo />
          </LogoDiv>

          <MenuDiv
            onClick={() => {
              setMenu(true)
              handleStateMobileMenu()
            }}
          >
            <MenuIcon />
          </MenuDiv>

          <Menu sown={menu}>
            <MenuData
              setMenu={setMenu}
              handleStateMobileMenu={handleStateMobileMenu}
            />
          </Menu>

          <ScrollDownLink
            className="animate__animated animate__bounce animate__repeat-5"
            onClick={e => scrollHandler(e)}
            href="#scroll-to-templates"
          >
            <ScrollIcon />
          </ScrollDownLink>

          <HeadTitle>{title || content.intro}</HeadTitle>
        </div>
        <ShortDetail id="scroll-to-templates">
          {content.description}
        </ShortDetail>
        <OurRoots content={content} />
        <Footer isSpecialTemplate />
      </Root>
    </>
  )
}

export default PageTemplate

export const pageQuery = graphql`
  query PageByID($id: String!) {
    markdownRemark(id: { eq: $id }) {
      id
      frontmatter {
        title
        description
        intro
        slug
        sectionBackgroundImg1
        sectionBackgroundImg2
        sectionFirstTopic
        sectionFirstTopicDetail
        sectionFirstTopicHeading1
        sectionFirstTopicHeading2
        sectionFirstTopicHeading3
        sectionFirstTopicImg1
        sectionFirstTopicImg2
        sectionFirstTopicImg3
        sectionLastImg
        sectionLastImgText
        sectionSecondTopic
        sectionSecondTopicDetail
        sectionSecondTopicPlan
        sectionThirdTopic
        sectionThirdTopicDetail
        sectionThirdTopicHeading
        sectionThirdTopicImg
        sliderImage {
          sliderBackground
        }
        slug
        seo {
          title
          description
          imageUrl
        }
      }
    }
  }
`
