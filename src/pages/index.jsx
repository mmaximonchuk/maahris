import React, { useState, useEffect } from "react"
import styled from "styled-components"
import Image from "../components/NetlifyCmsImage"
import Logo from "../images/assets/Logo.svg"
import MenuIcon from "../images/assets/MenuIcon.svg"
import ScrollIcon from "../images/assets/ScrollDown.svg"
import Content from "../components/Hompage/Content"
import Footer from "../components/Shared/Footer"
import MenuData from "../components/Shared/Menu"
import SEO from "../components/Shared/SEO"
import "animate.css/animate.css"
import "../styles/font.scss"
import { Link } from "gatsby"

import HomeData from "../../site/content/home.json"

const Root = styled.div`
  display: block;
  overflow-x: hidden;
`
const LogoDiv = styled(Link)`
  position: absolute;
  display: inline-block;
  top: 10%;
  left: 6%;
`
const MenuDiv = styled.div`
  position: absolute;
  top: 10%;
  right: 6%;
  height: 56px;
  align-items: center;
  display: flex;
  cursor: pointer;
`
const ScrollDownLink = styled(Link)`
  display: inline-block;
  position: absolute;
  bottom: 12vh;
  left: calc(50% - 24px);
  cursor: pointer;
`
const Menu = styled.div`
  position: absolute;
  right: ${props => (props.sown ? 0 : "-800px")};
  transition: right 0.5s;
  top: 0;
  height: 100vh;
  width: 467px;
  background-color: #14415a;
  z-index: 2;
  @media (max-width: 800px) {
    width: 100vw;
  }
`
const HeadTitle = styled.div`
  position: absolute;
  color: white;
  font-size: 68px;
  line-height: 90px;
  font-family: Nunito Sans;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  height: max-content;
  max-width: 1082px;
  text-align: center;
  @media (max-width: 800px) {
    font-size: 48px;
    line-height: 64px;
    max-width: 400px;
  }
  @media (max-width: 420px) {
    font-size: 28px;
    line-height: 37px;
    max-width: 245px;
  }
`
const IndexPage = ({ location }) => {
  const [menu, setMenu] = useState(false)
  const scrollHandler = e => {
    e.target.addEventListener("click", function (e) {
      e.preventDefault()
      const blockID = e.target.getAttribute("href")

      document.querySelector(blockID).scrollIntoView({
        behavior: "smooth",
        block: "start",
      })
    })
  }

  const [isOpenMobileMenu, setStateMobileMenu] = useState(false)

  const handleStateMobileMenu = () => {
    setStateMobileMenu(!isOpenMobileMenu)
    const body = document.querySelector("body")

    if (isOpenMobileMenu) {
      body.style.overflow = "auto"
    } else {
      body.style.overflow = "hidden"
    }
  }

  useEffect(() => {
    if (!isOpenMobileMenu) {
      const body = document.querySelector("body")
      body.style.overflow = "auto"
    }
  }, [isOpenMobileMenu])

  return (
    <>
      <SEO
        title={HomeData.seo.title}
        description={HomeData.seo.description}
        image={HomeData.seo.image}
        location={location.href}
      />
      <Root>
        <div className="max-h-screen relative center white">
          <Image
            imageUrl={HomeData.introHomePageBackground}
            className="object-cover"
            style={{ height: "100%" }}
          />
          <LogoDiv to="/">
            <Logo />
          </LogoDiv>
          <MenuDiv
            onClick={() => {
              setMenu(true)
              handleStateMobileMenu()
            }}
          >
            <MenuIcon />
          </MenuDiv>
          <Menu sown={menu}>
            <MenuData
              setMenu={setMenu}
              handleStateMobileMenu={handleStateMobileMenu}
            />
          </Menu>
          <ScrollDownLink
            onClick={e => scrollHandler(e)}
            href="#scroll-to-homepage"
          >
            <ScrollIcon className="animate__animated animate__bounce animate__repeat-2 animate__slow" />
          </ScrollDownLink>
          <HeadTitle>{HomeData.intro}</HeadTitle>
        </div>
        <Content id="scroll-to-homepage" sections={HomeData.sections} />
        <Footer />
      </Root>
    </>
  )
}

export default IndexPage
