import React from "react"
import styled from "styled-components"
import Image from "../../components/NetlifyCmsImage"

const Main = styled.div`
  display: block;
`
const Root = styled.div`
  display: flex;
  flex-direction: column;
  padding: 176px 80px 203px;
  background-color: #f5f5f5;
  @media (max-width: 1180px) {
    padding: 100px 80px;
  }
  @media (max-width: 500px) {
    padding: 80px 36px;
  }
`
const Content = styled.div`
  margin: auto;
  max-width: 1608px;
`
const First = styled.div`
  display: flex;
  font-size: 24px;
  color: #aaa599;
  font-family: Nunito Sans;
  @media (max-width: 800px) {
    flex-direction: column;
  }
  @media (max-width: 500px) {
    font-size: 18px;
  }
`
const Title = styled.div`
  flex: 1;
  font-weight: 700;
  letter-spacing: 3.2px;
  @media (max-width: 800px) {
    margin: 0 0 40px 0;
  }
  @media (max-width: 500px) {
    margin: 0 0 30px 0;
  }
`
const TitleSpecial = styled.div`
  flex: 1;
  font-weight: 700;
  letter-spacing: 4.8px;
  @media (max-width: 800px) {
    margin: 0 0 40px 0;
  }
  @media (max-width: 500px) {
    margin: 0 0 30px 0;
  }
`
const Description = styled.div`
  flex: 1;
`
const Second = styled.div`
  display: flex;
  font-size: 24px;
  margin-top: 150px;
  color: #aaa599;
  justify-content: space-between;
  @media (max-width: 1180px) {
    margin-top: 90px;
  }
  @media (max-width: 800px) {
    margin-top: 60px;
    flex-direction: column;
  }
  @media (max-width: 500px) {
    font-size: 18px;
    margin-top: 40px;
  }
`
const Pic = styled.div`
  flex: 1;
  font-size: 48px;
  line-height: 64px;
  font-family: univia-pro;
  flex-direction: column;
  max-width: 350px;
  max-height: 350px;
  overflow: hidden;
  filter: grayscale(100%);
  transition: all 0.3s;
  &:hover {
    color: #14415a;
    position: relative;
    transform: scale(1.1);
    ${"" /* transform: scale(1.1); */}
    filter: grayscale(0);
  }
  @media (max-width: 1300px) {
    max-width: 25vw;
    &:hover {
      ${"" /* max-height: 350px; */}
      transform: scale(1.1);
    }
  }
  @media (max-width: 1180px) {
    font-size: 35px;
    line-height: 47px;
  }
  @media (max-width: 800px) {
    height: 100vh;
    max-width: 350px;
    width: 100%;
    margin: auto;
    margin-bottom: 35px;
    &:hover {
      ${"" /* max-width: 380px; */}
      transform: scale(1.1);
    }
  }
  @media (max-width: 450px) {
    max-width: 280px;
    margin: 0 auto 35px 0;
    max-height: 350px;
    &:hover {
      transform: scale(1.1);
    }
  }
`
const PicTitle = styled.p`
  margin-bottom: 10px;
`
const Design = styled.div`
  display: flex;
  flex-direction: column;
  padding: 200px 80px 120px;
  @media (max-width: 1180px) {
    padding: 100px 80px 70px;
  }
  @media (max-width: 500px) {
    padding: 100px 40px 70px;
  }
`
const TitleDetail = styled.div`
  margin-top: 40px;
  font-weight: 500;
  margin-right: 80px;
  letter-spacing: 0;
  line-height: 1.6;
  @media (max-width: 1180px) {
    margin-right: 30px;
  }
  @media (max-width: 500px) {
    margin-right: unset;
    margin-top: 30px;
  }
`
const DesignDescription = styled.div`
  flex: 1;
  font-size: 48px;
  line-height: 1.2;
  color: #14415a;
  font-family: univia-pro;
  @media (max-width: 1180px) {
    font-size: 35px;
  }
  @media (max-width: 800px) {
    font-size: 40px;
  }
  @media (max-width: 500px) {
    font-size: 28px;
  }
`
const DesignImg = styled.div`
  display: flex;
  @media (max-width: 800px) {
    flex-direction: column;
  }
`
const ImgC = styled.div`
  flex: 1;
`
const Projects = styled.div`
  display: flex;
  flex-direction: column;
  padding: 200px 80px 180px;
  @media (max-width: 1180px) {
    padding: 100px 80px 80px;
  }
  @media (max-width: 500px) {
    padding: 80px 36px 0;
  }
`
const ProjectTitle = styled.div`
  font-size: 24px;
  line-height: 34px;
  font-weight: 700;
  font-family: Nunito Sans;
  margin-top: 0;
  margin-bottom: 75px;
  letter-spacing: 3.6px;
  color: #aaa599;
  @media (max-width: 500px) {
    font-size: 18px;
    line-height: 30px;
    margin-top: 0;
    margin-bottom: 40px;
  }
`
const ProjectDetail = styled.div`
  display: flex;
  @media (max-width: 800px) {
    flex-direction: column;
  }
`
const ProjectDetailone = styled.div`
  flex: 1;
  filter: grayscale(100%);
  transition: all 0.3s;
  overflow: hidden;
  max-height: 700px;
  &:hover {
    max-height: 720px;
    position: relative;
    transform: scale(1.05);
    filter: grayscale(0);
  }
  @media (max-width: 800px) {
    max-height: 462px;
    img {
      object-position: 0 -70px !important;
    }
    &:hover {
      max-height: 462px;
      position: relative;
      transform: scale(1.05);
      filter: grayscale(0);
    }
  }
  @media (max-width: 500px) {
    max-height: 250px;
    &:hover {
      max-height: 250px;
      position: relative;
      transform: scale(1.05);
      filter: grayscale(0);
    }
  }
`
const ProjectDetailtwo = styled.div`
  flex: 1;
`
const D1 = styled.div`
  margin-left: 15%;
  font-size: 24px;
  line-height: 40px;
  font-family: Nunito Sans;
  color: #aaa599;
  @media (max-width: 1180px) {
    margin-left: 40px;
  }
  @media (max-width: 800px) {
    margin-left: unset;
  }
  @media (max-width: 500px) {
    font-size: 18px;
    line-height: 30px;
    margin-bottom: 40px;
  }
`
const D1T = styled.div`
  font-size: 48px;
  font-family: univia-pro;
  line-height: 64px;
  color: #14415a;
  margin-bottom: 30px;
  @media (max-width: 800px) {
    font-size: 40px;
    line-height: 53px;
    margin-top: 60px;
  }
  @media (max-width: 500px) {
    font-size: 28px;
    line-height: 40px;
    margin-top: 45px;
    margin-bottom: 20px;
  }
`
const Footer = styled.div`
  display: block;
  max-height: 496px;
  position: relative;

  & .gatsby-image-wrapper {
    min-height: 496px;
    @media (max-width: 1180px) {
      min-height: 356px;
    }
    @media (max-width: 800px) {
      min-height: 300px;
    }
    @media (max-width: 500px) {
      min-height: 260px;
    }
  }
`
const FooterText = styled.div`
  position: absolute;
  color: white;
  font-size: 24px;
  font-family: Nunito Sans;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  height: max-content;
  text-align: center;
  max-width: 688px;
  line-height: 1.7;
  @media (max-width: 800px) {
    max-width: 70vw;
    font-size: 18px;
  }
`
const DevidedPar = styled(D1)`
  margin-top: 20px;
`

const OurRoots = props => {
  const { content } = props
  return (
    <Main>
      <Root>
        <Content>
          <First>
            <Title>{content.sectionFirstTopic}</Title>
            <Description>{content.sectionFirstTopicDetail}</Description>
          </First>
          <Second>
            <Pic>
              <PicTitle>{content.sectionFirstTopicHeading1}</PicTitle>
              <Image
                imageUrl={content.sectionFirstTopicImg1}
                className="object-cover"
              />
            </Pic>
            <Pic>
              <PicTitle>{content.sectionFirstTopicHeading2}</PicTitle>
              <Image
                imageUrl={content.sectionFirstTopicImg2}
                className="object-cover"
              />
            </Pic>
            <Pic>
              <PicTitle>{content.sectionFirstTopicHeading3}</PicTitle>
              <Image
                imageUrl={content.sectionFirstTopicImg3}
                className="object-cover"
              />
            </Pic>
          </Second>
        </Content>
      </Root>
      <Design>
        <Content>
          <First>
            <TitleSpecial>
              {content.sectionSecondTopic}
              <TitleDetail>{content.sectionSecondTopicDetail}</TitleDetail>
            </TitleSpecial>
            <DesignDescription>
              {content.sectionSecondTopicPlan}
            </DesignDescription>
          </First>
        </Content>
      </Design>
      <DesignImg>
        <ImgC>
          <Image
            imageUrl={content.sectionBackgroundImg1}
            className="object-cover"
          />
        </ImgC>
        <ImgC>
          <Image
            imageUrl={content.sectionBackgroundImg2}
            className="object-cover"
          />
        </ImgC>
      </DesignImg>
      <Projects>
        <Content>
          <ProjectTitle>{content.sectionThirdTopic}</ProjectTitle>
          <ProjectDetail>
            <ProjectDetailone>
              <Image
                imageUrl={content.sectionThirdTopicImg}
                className="object-cover"
              />
            </ProjectDetailone>
            <ProjectDetailtwo>
              <D1>
                <D1T>{content.sectionThirdTopicHeading}</D1T>
                <div
                  dangerouslySetInnerHTML={{
                    __html: content.sectionThirdTopicDetail,
                  }}
                />
                <DevidedPar>{content.sectionThirdTopicDetail2}</DevidedPar>
              </D1>
            </ProjectDetailtwo>
          </ProjectDetail>
        </Content>
      </Projects>
      <Footer>
        <Image imageUrl={content.sectionLastImg} className="object-cover" />
        <FooterText>{content.sectionLastImgText}</FooterText>
      </Footer>
    </Main>
  )
}

export default OurRoots
