import React, { useState, useEffect } from "react"
import { Link } from "gatsby"
import styled from "styled-components"
import Facebook from "../../images/assets/facebook.svg"
import Instagram from "../../images/assets/instagram-glyph-1.svg"
import Twitter from "../../images/assets/twitter.svg"
import MenuClose from "../../images/assets/MenuCloseIcon.svg"

const MenuCloseIcon = styled(MenuClose)`
  position: absolute;
  right: 122px;
  top: 8%;
  cursor: pointer;
  @media (max-width: 420px) {
    right: 40px;
    top: 5%;
    height: 24px;
  }
`
const Content = styled.div`
  display: flex;
  flex-direction: column;
  ${"" /* padding-top: 145px;
  padding-left: 122px; */}
  padding-right: 50px;
  padding-top: 20vh;
  padding-bottom: 5vh;
  padding-left: 21%;
  height: 100vh;
  font-family: Nunito Sans;
  @media (max-width: 1180px) {
  }
  @media (max-width: 800px) {
    padding-top: 220px;
    padding-left: 100px;
  }
  @media (max-width: 800px) {
    padding-top: 180px;
  }
  @media (max-width: 500px) {
    padding-left: 60px;
    padding-top: 120px;
  }
  @media (max-width: 360px) {
    padding-top: 80px;
  }
`
const B = styled(Link)`
  margin-bottom: 5vh;
  font-size: 28px;
  color: white;
  font-family: univia-pro;
  @media (max-width: 420px) {
    font-size: 24px;
    margin-bottom: 40px;
  }

  &.active {
    color: #707070;
    text-decoration: none;
  }
`

const Icons = styled.div`
  display: flex;
  margin-bottom: 30px;
  @media (min-width: 700px) and (max-width: 800px) {
    padding-right: 15px;
  }
`
const Social = styled.a`
  padding-right: 25px;
`
const Phone = styled.a`
  color: white;
  font-size: 20px;
  display: inline-block;
  &:not(:first-child) {
    margin-top: 18px;
  }
  @media (min-width: 700px) and (max-width: 800px) {
    &:not(:first-child) {
      margin-top: 0px;
      margin-left: 30px;
    }
  }
  @media (max-width: 420px) {
    font-size: 18px;
    &:not(:first-child) {
      margin-top: 10px;
    }
  }
`
const Detail = styled.div`
  ${"" /* position: absolute; */}
  bottom: 70px;
  margin-top: auto;
  @media (min-width: 700px) and (max-width: 800px) {
    display: flex;
    bottom: 80px;
  }
  @media (max-width: 700px) {
    bottom: 80px;
  }
  @media (max-width: 360px) {
    bottom: 50px;
  }
`
const DetailsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  @media (min-width: 700px) and (max-width: 800px) {
    display: flex;
    flex-direction: row;
  }
  @media (max-width: 420px) {
    margin-top: 20px;
  }
`
const Footer = props => {
  const useHandler = () => {
    props.setMenu(false)
    props.handleStateMobileMenu()
  }

  return (
    <div>
      <MenuCloseIcon onClick={useHandler} />
      <Content>
        <B
          onClick={props.handleStateMobileMenu}
          to="/"
          activeClassName="active"
        >
          Home
        </B>
        <B
          onClick={props.handleStateMobileMenu}
          to="/restoration"
          activeClassName="active"
        >
          Restoration
        </B>
        <B
          onClick={() => {
            props.handleStateMobileMenu()
            props.setMenu(false)
          }}
          to="/#research"
          activeClassName="active"
        >
          Research
        </B>
        <B
          onClick={() => {
            props.handleStateMobileMenu()
            props.setMenu(false)
          }}
          to="/#visual-story-telling"
          activeClassName="active"
        >
          Visual Story Telling
        </B>
        <Detail>
          {/* <Icons>
            <Social target="_blank" href="https://www.twitter.com">
              <Twitter />
            </Social>
            <Social target="_blank" href="https://www.instagram.com">
              <Instagram />
            </Social>
            <Social target="_blank" href="https://www.facebook.com">
              <Facebook />
            </Social>
          </Icons> */}
          <DetailsWrapper>
            {/* <Phone href="tel:7862453590">786-245-3590</Phone> */}
            <Phone href="mailto:info@maahris.com">info@maahris.com</Phone>
          </DetailsWrapper>
        </Detail>
      </Content>
    </div>
  )
}
export default Footer
