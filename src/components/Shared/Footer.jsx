import React from "react"
import styled from "styled-components"
import Facebook from "../../images/assets/facebook.svg"
import Instagram from "../../images/assets/instagram-glyph-1.svg"
import Twitter from "../../images/assets/twitter.svg"

const Root = styled.div`
  display: flex;
  background-color: #14415a;
  color: white;
  padding: 150px 150px 130px;
  @media (max-width: 1180px) {
    padding: 84px 80px 76px;
    flex-direction: column-reverse;
  }
  @media (max-width: 500px) {
    padding: 60px 40px 65px;
    flex-direction: column-reverse;
  }
  @media (max-width: 420px) {
    padding: 60px 40px 65px;
    position: relative;
  }
`
const W = styled.div`
  flex: 1;
  text-align: center;
  font-size: 24px;
  font-family: Nunito Sans;
  font-weight: 500;
  display: flex;
  align-items: center;
  @media (max-width: 1180px) {
    flex-direction: row-reverse;
    ${"" /* justify-content: space-between;  temporary*/}
    justify-content: center;
  }
  @media (max-width: 621px) {
    font-size: 18px;
  }
  @media (max-width: 420px) {
    font-size: 14px;
  }
`
const NewSpecialFooter = styled.div`
  display: none;
  @media (max-width: 420px) {
    position: absolute;
    top: 0;
    left: 0;
    padding: 73px 0 73px;
    background: #14415a;
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
  }
`
const FooterSpecialDescription = styled.a`
  @media (max-width: 420px) {
    display: inline-block;
    color: #ffffff;
    font-size: 14px;
    font-family: Nunito Sans;
    &:not(:first-child) {
      margin-top: 28px;
    }
  }
`
const Address = styled(W)`
  ${"" /* justify-content: flex-end; temporary */}
  justify-content: center;
  @media (max-width: 1180px) {
    display: inline-block;
    margin-bottom: 40px;
    text-align: center;
    margin-right: auto;
    margin-left: auto;
  }
`
const Social = styled.a`
  ${"" /* padding-left: 15px;
  padding-right: 15px; */}
  &:not(:first-child) {
    padding-left: 30px;
  }
  @media (max-width: 420px) {
    &:not(:first-child) {
      padding-left: 16px;
    }
    ${"" /* padding-left: 16px; */}
  }
  ${
    "" /* @media (max-width: 420px) {
    padding-left: 8px;
    padding-right: 8px;
  } */
  }
`
const Icons = styled.div`
  padding-left: 70px;
  display: flex;
  @media (max-width: 1180px) {
    padding-left: 0px;
  }
`
const Footer = ({ isSpecialTemplate = false }) => {
  const address = "786-245-3590   |   78 Park Way, Berkeley, RI"

  return (
    <>
      {!isSpecialTemplate ? (
        <Root>
          <W>
            ©2021 Maāhris Holding Limited
            {/* <Icons>
              <Social target="_blank" href="https://www.twitter.com">
                <Twitter />
              </Social>
              <Social target="_blank" href="https://www.instagram.com">
                <Instagram />
              </Social>
              <Social target="_blank" href="https://www.facebook.com">
                <Facebook />
              </Social>
            </Icons> */}
          </W>
          {/* <Address>{address}</Address> */}
        </Root>
      ) : (
        <Root>
          <W>
            ©2021 Maāhris Holding Limited
            {/* <Icons>
              <Social target="_blank" href="https://www.twitter.com">
                <Twitter />
              </Social>
              <Social target="_blank" href="https://www.instagram.com">
                <Instagram />
              </Social>
              <Social target="_blank" href="https://www.facebook.com">
                <Facebook />
              </Social>
            </Icons>        */}
          </W>
          {/* <Address>{address}</Address>
          <NewSpecialFooter>
            <FooterSpecialDescription
              target="_blank"
              target="_blank"
              href="https://www.google.com/maps/search/68+Circular+Rd,+02,+%D0%A1%D0%B8%D0%BD%D0%B3%D0%B0%D0%BF%D1%83%D1%80+049422/@1.2864332,103.846946,17z/data=!3m1!4b1"
            >
              {"68 Circular Road, #02-01, 049422, Singapore"}
            </FooterSpecialDescription>
            <FooterSpecialDescription>
              {"©2020 Maāhris Pte. Ltd."}
            </FooterSpecialDescription>
          </NewSpecialFooter> */}
        </Root>
      )}
    </>
  )
}
export default Footer
