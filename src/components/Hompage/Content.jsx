import React from "react"
import Section from "./Sections"

const FirstPageContent = ({ sections, id }) => {
  let sectionsNewArray = []
  for (let i = 0; i < sections.length; i++) {
    sectionsNewArray.push({
      section: sections[i],
      id: i + 1,
    })
  }

  return (
    <div id={id}>
      {sectionsNewArray.map(section => {
        if (section.id % 2 == 0) {
          return (
            <Section
              key={section.id}
              isEven
              hideLink
              anchorToResearch
              content={section.section}
            />
          )
        }
        if (section.id === 3) {
          return (
            <Section
              key={section.id}
              hideLink
              anchorToStory
              content={section.section}
            />
          )
        }
        return <Section key={section.id} content={section.section} />
      })}
    </div>
  )
}

export default FirstPageContent
