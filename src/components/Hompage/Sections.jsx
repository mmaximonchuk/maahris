import React from "react"
import styled from "styled-components"
import { Link } from "gatsby"
import Image from "../NetlifyCmsImage"
import CTA from "../../images/assets/CTA.svg"

const Root = styled.div`
  padding: 180px 140px;
  display: flex;
  overflow: hidden;
  @media (max-width: 1180px) {
    padding: 180px 40px 130px;
  }
  @media (max-width: 800px) {
    flex-direction: column;
    padding: 170px 40px 110px;
    padding-right: 0px;
  }
  @media (max-width: 420px) {
    padding: 0px;
  }
`
const ImgContainer = styled.div`
  flex: 2;
  padding: 0px 110px;
  position: relative;
  display: block;
  @media (max-width: 1024px) {
    padding: 0px 10%;
  }
  @media (max-width: 800px) {
    padding-right: 0px;
    padding-bottom: 80px;
  }
  @media (max-width: 420px) {
    padding: 0px;
  }
`
const Detail = styled.div`
  flex: 1;
`
const Data = styled.div`
  max-width: 348px;
  margin: auto;
  font-size: 24px;
  line-height: 30px;
  height: 100%;
  font-family: Nunito Sans;
  color: #14415a;
  display: flex;
  flex-direction: column;
  @media (max-width: 1324px) {
    font-size: 20px;
    line-height: 25px;
  }
  @media (max-width: 800px) {
    max-width: 60vw;
    margin-left: 10%;
    font-size: 24px;
    line-height: 25px;
  }
  @media (max-width: 500px) {
    max-width: 80vw;
    font-size: 18px;
    line-height: 25px;
    margin-right: 40px;
  }
  @media (max-width: 420px) {
    padding: 0px;
    margin-bottom: 110px;
    margin-left: 47px;
  }
`
const Title = styled.div`
  position: absolute;
  font-size: 66px;
  line-height: 100px;
  font-family: univia-pro;
  color: #aaa599;
  max-width: 400px;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin-top: auto;
  margin-bottom: auto;
  height: max-content;
  @media (max-width: 800px) {
    font-size: 48px;
    left: -17px;
  }
  @media (max-width: 420px) {
    position: static;
    font-size: 32px;
    line-height: 40px;
    max-width: 80vw;
    margin: 40px auto 30px 47px;
  }
`
const CTAIcon = styled(CTA)`
  @media (max-width: 420px) {
    width: 190px;
  }
`
const LinkModified = styled(Link)`
  text-decoration: none;
  margin-top: auto;
  padding-top: 20px;
  display: inline-block;
  &:hover svg g text {
    fill: #aaa599;
    transition: all 0.3s;
  }
  &:hover svg g g g line {
    transition: all 0.3s;
    stroke: #aaa599;
  }
  &:hover svg g g g path {
    transition: all 0.3s;
    stroke: #aaa599;
  }

  @media (max-width: 800px) {
    padding-top: 0;
    margin-top: 30px;
  }
`

const FirstPageTemplate = ({ content, isEven = false,anchorToResearch = false,anchorToStory = false, hideLink = false }) => {
  const { title, description, photo, slug } = content
  return (
    <>
      <Root
        className={`animate__animated animate__fadeInLeft ${
          !isEven ? "" : "even-section-background"
        }`}
        id={`${!anchorToResearch ? '' : '#research'}${!anchorToStory ? '' : '#visualstorytelling'} `}
      >
        <ImgContainer>
          <Image imageUrl={photo} className="object-cover" />
          <Title>{title}</Title>
        </ImgContainer>

        <Detail>
          <Data>
            {description}
            {!hideLink && <LinkModified to={`/${slug}`}>
              <CTAIcon />
            </LinkModified>}
          </Data>
        </Detail>
      </Root>
    </>
  )
}

export default FirstPageTemplate
